/*
 * Copyright 2015 Gabriel Rodriguez
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *       Unless required by applicable law or agreed to in writing, software
 *       distributed under the License is distributed on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *       See the License for the specific language governing permissions and
 *       limitations under the License.
 */

package concretesolutions.com.desafioandroid.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import concretesolutions.com.desafioandroid.utils.Constants;

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class Repository implements Serializable {

    private int index;
    private String name;
    private String description;
    @SerializedName(Constants.API_REPOSITORY_URL)
    private String url;
    @SerializedName(Constants.API_REPOSITORY_FORKS)
    private int forks;
    @SerializedName(Constants.API_REPOSITORY_STARS)
    private int stars;
    private User owner;
    private ArrayList<PullRequest> pullRequestList;

    public Repository(int index, String name, String description, String url, int forks, int stars, User owner) {
        this.index = index;
        this.name = name;
        this.description = description;
        this.url = url;
        this.forks = forks;
        this.stars = stars;
        this.owner = owner;
    }

    public Repository(String name, String description, String url, int forks, int stars, User owner) {
        this.name = name;
        this.description = description;
        this.url = url;
        this.forks = forks;
        this.stars = stars;
        this.owner = owner;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public ArrayList<PullRequest> getPullRequestList() {
        return pullRequestList;
    }

    public void setPullRequestList(ArrayList<PullRequest> pullRequestList) {
        this.pullRequestList = pullRequestList;
    }

    @Override
    public String toString() {
        return "Repository{" +
                "index=" + index +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", forks=" + forks +
                ", stars=" + stars +
                ", owner=" + owner +
                ", pullRequestList=" + pullRequestList +
                '}';
    }
}
