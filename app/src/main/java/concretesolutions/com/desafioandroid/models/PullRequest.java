/*
 * Copyright 2015 Gabriel Rodriguez
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *       Unless required by applicable law or agreed to in writing, software
 *       distributed under the License is distributed on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *       See the License for the specific language governing permissions and
 *       limitations under the License.
 */

package concretesolutions.com.desafioandroid.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import concretesolutions.com.desafioandroid.utils.Constants;

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class PullRequest implements Serializable {

    private int id;
    @SerializedName(Constants.API_PULL_REQUEST_NAME)
    private String name;
    private String state;
    @SerializedName(Constants.API_PULL_REQUEST_DESCRIPTION)
    private String description;
    @SerializedName(Constants.API_REPOSITORY_URL)
    private String url;
    private User user;

    public PullRequest(int id, String name, String state, String description, String url, User user) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.description = description;
        this.url = url;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "PullRequest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", user=" + user +
                '}';
    }
}
