/*
 * Copyright 2015 Gabriel Rodriguez
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *       Unless required by applicable law or agreed to in writing, software
 *       distributed under the License is distributed on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *       See the License for the specific language governing permissions and
 *       limitations under the License.
 */

package concretesolutions.com.desafioandroid.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;

import java.util.List;

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class InviteBundle {

    private final String LOG_TAG = getClass().getSimpleName();
    /**
     * Activity params
     */
    private Context context;
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Most popular Facebook apps available on the Play Store.
     */
    String[] FACEBOOK_APPS = {
            "com.facebook.katana"
    };

    /**
     * Most popular WhatsApp apps available on the Play Store.
     */
    String[] WHATSAPP_APPS = {
            "com.whatsapp"
    };

    public InviteBundle(Context context, String message) {
        this.context = context;
        this.message = message;
    }

    public void shareOnFacebook() {
        Log.d(LOG_TAG, "Sharing on Facebook");
        if (FieldValidator.isValid(message)) {
            Intent facebook = findClients(message, FACEBOOK_APPS);
            context.startActivity(facebook);
        }
    }

    public void shareOnWhatsApp() {
        Log.d(LOG_TAG, "Sharing on WhatsApp");
        if (FieldValidator.isValid(message)) {
            Intent whatsapp = findClients(message, WHATSAPP_APPS);
            context.startActivity(whatsapp);
        }
    }

    public void shareOnEmail() {
        Log.d(LOG_TAG, "Sharing on Email");
        if (FieldValidator.isValid(message)) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_TEXT, message);
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            }
        }

    }

    private Intent findClients(String message, String[] apps) {
        Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.setType("text/plain");
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(
                intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (int i = 0; i < apps.length; i++) {
            for (ResolveInfo resolveInfo : list) {
                String p = resolveInfo.activityInfo.packageName;
                if (p != null && p.startsWith(apps[i])) {
                    intent.setPackage(p);
                    return intent;
                }
            }
        }
        intent.setAction(Intent.ACTION_SEND);
        return intent;
    }


}
