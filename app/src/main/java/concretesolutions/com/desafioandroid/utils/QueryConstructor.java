/*
 * Copyright 2015 Gabriel Rodriguez
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *       Unless required by applicable law or agreed to in writing, software
 *       distributed under the License is distributed on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *       See the License for the specific language governing permissions and
 *       limitations under the License.
 */

package concretesolutions.com.desafioandroid.utils;

import android.net.Uri;
import android.util.Log;

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class QueryConstructor {

    private final String LOG_TAG = getClass().getSimpleName();

    public QueryConstructor() {
    }

    /**
     * Allows to construct the query for Repositories web services
     *
     * @param page repository query page
     * @return build url
     */
    public String buildRepositoryQuery(String page) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Constants.API_SCHEME)
                .authority(Constants.API_AUTHORITY)
                .appendPath(Constants.API_PATH_SEARCH)
                .appendPath(Constants.API_PATH_REPOSITORIES)
                .appendQueryParameter(Constants.API_PARAM_QUERY_Q, Constants.API_PARAM_VALUE_LANGUAGE_JAVA)
                .appendQueryParameter(Constants.API_PARAM_QUERY_SORT, Constants.API_PARAM_VALUE_STARS)
                .appendQueryParameter(Constants.API_PARAM_QUERY_PAGE, page);
        String url = builder.build().toString();
        Log.d(LOG_TAG, url);
        return url;
    }

    /**
     * Allows to construct the query for Pull Request web service
     *
     * @param repositoryName Name of repository
     * @param ownerName      Name of repository owner
     * @return build url
     */
    public String buildPullRequestQuery(String repositoryName, String ownerName) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Constants.API_SCHEME)
                .authority(Constants.API_AUTHORITY)
                .appendPath(Constants.API_PATH_REPOS)
                .appendPath(ownerName)
                .appendPath(repositoryName)
                .appendPath(Constants.API_PATH_PULL);
        String url = builder.build().toString();
        Log.d(LOG_TAG, url);
        return url;
    }
}
