/*
 * Copyright 2015 Gabriel Rodriguez
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *       Unless required by applicable law or agreed to in writing, software
 *       distributed under the License is distributed on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *       See the License for the specific language governing permissions and
 *       limitations under the License.
 */

package concretesolutions.com.desafioandroid.utils;

import android.widget.EditText;

import java.util.List;

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class FieldValidator {

    public static boolean isValid(EditText field) {
        return field.getText() != null
                && field.getText().toString().trim() != null
                && !field.getText().toString().trim().isEmpty();
    }

    public static boolean isValid(List<?> list) {
        return list != null && !list.isEmpty();
    }

    public static boolean isValid(String string) {
        return string != null && !string.trim().isEmpty() && !string.equalsIgnoreCase("null");
    }
}

