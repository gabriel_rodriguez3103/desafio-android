/*
 * Copyright 2015 Gabriel Rodriguez
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *       Unless required by applicable law or agreed to in writing, software
 *       distributed under the License is distributed on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *       See the License for the specific language governing permissions and
 *       limitations under the License.
 */

package concretesolutions.com.desafioandroid.utils;

/**
 * @author Gabriel Rodriguez.
 * @version 1.0
 */
public class Constants {

    /**
     * GitHub Url
     */
    public static final String url = "https://github.com/";
    /**
     * Splash Time in milli seconds
     */
    public static final int SPLASH_INTERVAL = 3000;
    /**
     * Page Indicators
     */
    public static final int PRINCIPAL_PAGE_ID = 0x00;
    public static final int SECONDARY_PAGE_ID = 0x01;

    /**
     * Query Indicators
     */
    public static final int REPOSITORIES_QUERY_ID = 0x01;
    public static final int PULL_REQUEST_QUERY_ID = 0x02;

    /**
     * Api Paths
     */
    public static final String API_SCHEME = "https";
    public static final String API_AUTHORITY = "api.github.com";
    public static final String API_PATH_SEARCH = "search";
    public static final String API_PATH_REPOSITORIES = "repositories";
    public static final String API_PATH_REPOS = "repos";
    public static final String API_PATH_PULL = "pulls";
    public static final String API_PARAM_QUERY_Q = "q";
    public static final String API_PARAM_QUERY_SORT = "sort";
    public static final String API_PARAM_QUERY_PAGE = "page";
    public static final String API_PARAM_VALUE_LANGUAGE_JAVA = "language:Java";
    public static final String API_PARAM_VALUE_STARS = "stars";

    /**
     * Bundle parameters
     */
    public static final String TAG_REPOSITORIES_LIST = "repositories_list";
    public static final String TAG_PULL_REQUEST_LIST = "pull_request_list";

    /**
     * Api Params Key
     */
    // Repositories
    public static final String API_REPOSITORY_FULL_NAME = "full_name";
    public static final String API_REPOSITORY_URL = "html_url";
    public static final String API_REPOSITORY_FORKS = "forks_count";
    public static final String API_REPOSITORY_STARS = "stargazers_count";
    // User
    public static final String API_USER_NAME = "login";
    public static final String API_USER_PHOTO = "avatar_url";
    // Pull Request
    public static final String API_PULL_REQUEST_NAME = "title";
    public static final String API_PULL_REQUEST_DESCRIPTION = "body";

    /**
     * Pull Request States
     */
    public static final String TAG_STATE_OPEN = "open";
    public static final String TAG_STATE_CLOSE = "close";

}
