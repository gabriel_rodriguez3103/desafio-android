package concretesolutions.com.desafioandroid.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import concretesolutions.com.desafioandroid.R
import concretesolutions.com.desafioandroid.activity.MainActivity
import concretesolutions.com.desafioandroid.helpers.ConnectionManager
import concretesolutions.com.desafioandroid.utils.Constants

class SplashActivity : FragmentActivity() {

    private val LOG_TAG = javaClass.simpleName


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)

        if (ConnectionManager.isConnected(this)!!) {
            /**
             * Time Before Display Main Activity
             */
            Handler().postDelayed({
                Log.d(LOG_TAG, "Launching Main Activity")
                val mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(mainIntent)
                finish()
            }, Constants.SPLASH_INTERVAL.toLong())

        } else {
            Log.d(LOG_TAG, "Showing Confirmation Dialog")
        }

    }
}